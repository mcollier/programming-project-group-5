class Widget {
  int x, y, width, height;
  String label;
  int event;
  color widgetColor, labelColor;
  PFont widgetFont;
  boolean hover = false;

  Widget(int x, int y, int width, int height, String label, 
  color widgetColor, PFont widgetFont, int event) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.label = label;
    this.event = event;
    this.widgetColor = widgetColor;
    this.widgetFont = widgetFont;
    labelColor = color(255);
  }

  void draw() {
    if (hover) {
      stroke(255);
    } else {
      stroke(0);
    }

    fill(widgetColor);
    rect(x, y, width, height);
    fill(labelColor);
    text(label, x+10, y+height-10);
  }

  int getEvent(int mX, int mY) {
    if (mX>x && mX < x+width && mY > y && mY < y+height) {
      return event;
    }
    return EVENT_NULL;
  }
}

class TextBox extends Widget {
  int maxlen;

  TextBox(int x, int y, int width, int height, String label, 
  color widgetColor, PFont widgetFont, int event, int maxlen) {
    super(x, y, width, height, label, widgetColor, widgetFont, event);   

    labelColor  =  color(0); 
    this.maxlen  =  maxlen;
  }

  void append(char s) {
    if (s ==  BACKSPACE) {  

      if (!label.equals("")) {

        label  =  label.substring(0, label.length()-1);
      }
    } else  if (label.length()  <  maxlen) {

      label  =  label  +  str(s);
    }
  }

  String getText() {
    return label;
  }

  void clearText() {
    label = "";
  }
}

