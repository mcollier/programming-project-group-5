import grafica.*;

Data data;
ArrayList<Data> dataList = new ArrayList<Data>();
ArrayList<Widget> widgets = new ArrayList<Widget>();
char[] medallionCharacters = {
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
  'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', BACKSPACE
};

String allInstances ="";

int nPoints;
GPointsArray points;

GradientMap map;

final int EVENT_NULL = 0;
final int EVENT_BUTTON1 = 1;
final int EVENT_BUTTON2 = 2;
final int EVENT_BUTTON3 = 3;
final int EVENT_BUTTON4 = 4;
final int EVENT_BUTTON5 = 5;
final int EVENT_BUTTON6 = 6;
final int EVENT_BUTTON7 = 7;
final int EVENT_BUTTON8 = 8;
final int EVENT_TEXT_MEDALLION = 9;
final int EVENT_SEARCH_MEDALLION = 10;
final int EVENT_RESET_SELECT = 11;

final int query_default = 0;
final int query1 = 1;
final int query2 = 2;
final int query3 = 3;
final int query4 = 4;
final int query5 = 5;
final int query6 = 6;
final int query7 = 7;

int current_query = query_default;

final int noSearch = 0;
final int medallionSearch = 1;

int search_item = noSearch;

color firstColor = color(255);
color secondColor = color(200);

Widget widget1;
Widget widget2;
Widget widget3;
Widget widget4;
Widget widget5;
Widget widget6;
Widget widget7;
Widget widget8;

TextBox medallionBox;
TextBox focus;
Widget medallionWidget;
Widget resetSelectWidget;

final int SCREEN_WIDTH = 1000;
final int SCREEN_HEIGHT = 650;

int currentTime;
int count;

boolean displayTimeMarkers = false;
boolean viewTrip = false;
boolean validCharacter = false;
boolean markerSelected = false;

String timeDisplay;

TaxiMarker plotMarkerOne;
TaxiMarker plotMarkerTwo;

PImage bg;
PImage bgMenu;

void setup() 
{
  bg = loadImage("NYC Fade.jpg");
  bgMenu = loadImage("resized-image-3.jpg");
  // loads file, prints it length in lines and loops through the lines printing the 6th column
  String lines[] = loadStrings("mediumTripData.csv");
  println("there are " + lines.length + " lines");

  for (int i=0; i<lines.length; i++) {
    try {
      String[] tokens = split(lines[i], ',');
      if (cleanData(float(tokens[10]), float(tokens[11]), float(tokens[12]), 
      float(tokens[13]), int(tokens[7]), int(tokens[8]), float(tokens[9]), tokens[0], tokens[1], 
      tokens[2], tokens[4], tokens[5], tokens[6])) {

        Data data = new Data(tokens[0], tokens[1], tokens[2], int(tokens[3]), tokens[4], tokens[5], tokens[6], 
        int(tokens[7]), int(tokens[8]), float(tokens[9]), float(tokens[10]), float(tokens[11]), float(tokens[12]), 
        float(tokens[13]));
        dataList.add(data);
      } else {
        println("Unclean data at line " + i);
      }
    }
    catch(Exception e) {
      // exception handler to deal with any corrupted rows of data
      println("Error at line: " + i);
    }
  }

  for (int index=0; index<dataList.size (); index++)
  {
    Data dataInstance = dataList.get(index);
    allInstances = allInstances + dataInstance.toString() + "\n";
  }

  PFont stdFont = createFont("Arial", 20, true);
  textFont(stdFont);
  textAlign(LEFT, BOTTOM);

  widget1 = new Widget(5, 260, 100, 40, "Trip Distances", 
  color(#2196F3), stdFont, EVENT_BUTTON1);
  widgets.add(widget1);

  widget2 = new Widget(5, 300, 100, 40, "Trip Times", 
  color(#2196F3), stdFont, EVENT_BUTTON2);
  widgets.add(widget2);

  widget3 = new Widget(5, 340, 100, 40, "Map view", 
  color(#2196F3), stdFont, EVENT_BUTTON3);
  widgets.add(widget3);

  widget4 = new Widget(5, 380, 100, 40, "Dist. Bar", 
  color(#2196F3), stdFont, EVENT_BUTTON4);
  widgets.add(widget4);

  widget5 = new Widget(5, 600, 100, 40, "Main Menu", 
  color(#2196F3), stdFont, EVENT_BUTTON5);
  widgets.add(widget5);

  widget6 = new Widget(5, 420, 100, 40, "Passengers", 
  color(#2196F3), stdFont, EVENT_BUTTON6);
  widgets.add(widget6);

  widget7 = new Widget(5, 460, 100, 40, "View Times", 
  color(#2196F3), stdFont, EVENT_BUTTON7);
  widgets.add(widget7);

  widget8 = new Widget(5, 500, 100, 40, "Select a Taxi", 
  color(#2196F3), stdFont, EVENT_BUTTON8);
  widgets.add(widget8);

  medallionBox = new TextBox(200, 600, 550, 40, "", 
  color(255), stdFont, EVENT_TEXT_MEDALLION, 40);

  
  focus = null;

  medallionWidget = new Widget(800, 600, 150, 40, "Search", 
  color(150, 150, 250), stdFont, EVENT_SEARCH_MEDALLION);
  
  resetSelectWidget = new Widget(5, 540, 100, 40, "Reset Select",
  color(150, 150, 250), stdFont, EVENT_RESET_SELECT);

  size(SCREEN_WIDTH, SCREEN_HEIGHT);

  map = new GradientMap(this, dataList);
}

void mousePressed() {
  int event = EVENT_NULL;

  if (widgets != null) {
    for (int i=0; i<widgets.size (); i++) {
      Widget theWidget = widgets.get(i);
      event = theWidget.getEvent(mouseX, mouseY);

      switch (event) {
      case EVENT_BUTTON1:
        current_query = query1;
        break;
      case EVENT_BUTTON2:
        current_query = query2;
        break;
      case EVENT_BUTTON3:
        map = new GradientMap(this, dataList);
        displayTimeMarkers = false;
        current_query = query3;
        break;
      case EVENT_BUTTON4:
        current_query = query4;
        break;
      case EVENT_BUTTON5:
        map = new GradientMap(this, dataList);
        displayTimeMarkers = false;
        focus = null;
        search_item = noSearch;
        viewTrip = false;
        markerSelected = false;
        current_query = query_default;
        break;
      case EVENT_BUTTON6:
        current_query = query5;
        break;
      case EVENT_BUTTON7:
        currentTime = 0000;
        count = 0;
        current_query = query6;
        break;
      case EVENT_BUTTON8:
        medallionBox.clearText();
        current_query = query7;
        break;
      }
    }
    if (medallionBox != null && medallionWidget != null) {
      event = medallionBox.getEvent(mouseX, mouseY); 
      if (event == EVENT_TEXT_MEDALLION) {
        focus = medallionBox; 
        medallionBox.hover = true;
        println("clicked  on  the text box!");
      }

      event = medallionWidget.getEvent(mouseX, mouseY);
      if (event == EVENT_SEARCH_MEDALLION) {
        search_item = medallionSearch;
        //medallionWidget.hover = true;
        
        println("clicked on the search button!");
      }
      
      event = resetSelectWidget.getEvent(mouseX, mouseY);
      if (event == EVENT_RESET_SELECT) {
        map = new GradientMap(this, dataList);
        medallionBox.clearText();
        focus = null;
        search_item = noSearch;
        viewTrip = false;
        markerSelected = false;
        current_query = query7;        
      }
    }
  }
  if (viewTrip) {
    ArrayList<Data> tripDataList = new ArrayList<Data>();
    plotMarkerOne = map.checkMarkers(mouseX, mouseY);
    if (plotMarkerOne != null)
    {
      plotMarkerOne.markerColor = color(0, 0, 0);

      for (int i=0; i<dataList.size (); i++) {
        Data dataItem = dataList.get(i);

        if (dataItem.pickupLatitude == plotMarkerOne.getLat() && dataItem.pickupLongitude == plotMarkerOne.getLon())
        {
          tripDataList.add(dataItem);
        } else if (dataItem.dropoffLatitude == plotMarkerOne.getLat() && dataItem.dropoffLongitude == plotMarkerOne.getLon())   
        {
          tripDataList.add(dataItem);
        }
      }
      
      if (tripDataList != null) {
        map = new GradientMap(this, tripDataList);
        for (int i=0; i<map.markerList.size (); i++) {
          TaxiMarker marker = map.markerList.get(i);          
          marker.setDisplay(map.taxiLocations);
          println(marker.markerInfo);
          markerSelected = true;
          viewTrip = false;
        }
        markerSelected = true;
        current_query = query3;
      }
    }
  }
}

void mouseMoved() {
}

void keyPressed() {
  if (focus != null) {
    for (int i =0; i<medallionCharacters.length; i++) {
      if (medallionCharacters[i] == key) {
        validCharacter = true;
      }
    }
    if (validCharacter) {
      focus.append(key);
    } else {
      println("Invalid character entered.");
    }    
    validCharacter = false;
  }
}

void draw() 
{
  background(bg);

  mouseOverWidget();
  stroke(0);

  switch(current_query) 
  {
  case query_default:
    background(bgMenu);
    strokeWeight(1.5);
    fill(#607D8B);
    rect(0, 250, 110, 400);
    stroke(0);
    strokeWeight(1);

    PFont f = createFont("Arial", 100, true);
    textFont(f, 14);

    if (widgets != null) 
    {
      for (int i=0; i< (widgets.size ()); i++) 
      {
        if (i != 4)
        {
          Widget theWidget = widgets.get(i);
          theWidget.draw();
        }
      }
    }
    break;

  case query1:
    nPoints = dataList.size() - 1;
    points = new GPointsArray(nPoints);

    for (int i = 1; i < nPoints; i++) {
      Data theData = dataList.get(i);
      points.add(i, theData.tripDistance);
    }
    GPlot plot = new GPlot(this);
    plot.setPos(25, 25);
    plot.setDim(800, 400);
    plot.setTitleText("Trip Distances");
    plot.getXAxis().setAxisLabelText("Journey Number");
    plot.getYAxis().setAxisLabelText("Distance");
    plot.setPoints(points);
    plot.defaultDraw();

    fill(200, 200, 200);

    rect(0, 590, 110, 60);
    Widget theWidget = widgets.get(4);
    theWidget.draw();
    break;

  case query2:  
    nPoints = dataList.size() - 1;
    points = new GPointsArray(nPoints);

    for (int i = 1; i < nPoints; i++) {
      Data theData = dataList.get(i);
      points.add(i, theData.tripTimeSecs);
    }
    GPlot plot2 = new GPlot(this);
    plot2.setPos(25, 25);
    plot2.setDim(800, 400);
    plot2.setDim(new float[] {
      0, 300
    }
    );

    plot2.getYAxis().setAxisLabelText("Time (secs)");
    plot2.getXAxis().setAxisLabelText("Journey number");
    plot2.getTitle().setText("Trip Times Histogram");
    plot2.setPoints(points);
    plot2.startHistograms(GPlot.VERTICAL);
    plot2.getHistogram().setDrawLabels(true);

    plot2.beginDraw();
    plot2.drawBackground();
    plot2.drawBox();
    plot2.drawYAxis();
    plot2.drawXAxis();
    plot2.drawTitle();
    plot2.drawHistograms();
    plot2.endDraw(); 

    fill(200, 200, 200);

    rect(0, 590, 110, 60);
    theWidget = widgets.get(4);
    theWidget.draw();
    break;

  case query3:
    //PFont f = createFont("Arial", 200, true);
    //textFont(f, 10.68);
    //fill(0);
    //text(allInstances, 10, 10, SCREEN_WIDTH-20, SCREEN_HEIGHT-20); 
    //PFont f = createFont("Arial", 200, true);
    //textFont(f, 10.68);
    //fill(0);
    //text(allInstances, 10, 10, SCREEN_WIDTH-20, SCREEN_HEIGHT-20); 
    map.draw();

    if (displayTimeMarkers)
    {
      if (currentTime < 100)
      {
        timeDisplay = "Time: " + String.format("%04d", currentTime);
      } else if (currentTime < 1000)
      {
        timeDisplay = "Time: " + String.format("%04d", currentTime);
      } else
      {
        timeDisplay = "Time: " + currentTime;
      }
      PFont g = createFont("Arial Bold", 200, true);
      textFont(g, 20);
      fill(0);
      text(timeDisplay, 50, 80); 
      //text(count, 700, 620); 

      PFont h = createFont("Arial", 100, true);
      textFont(h, 14);

      count++;

      if (count == 25 )
      {
        current_query = query6;
      }
    }

    if (markerSelected) {
      PFont g = createFont("Arial", 200, true);
      textFont(g, 13);
      fill(200, 200, 200);
      rect(0, 530, 1000, 120);
      
      resetSelectWidget.draw();

      g = createFont("Arial Bold", 200, true);
      textFont(g, 15);
      fill(0);

      for (int i=0; i<map.taxiLocations.size (); i++) {
        Data taxi = map.taxiLocations.get(i);          

        text("Taxi Data", 480, 570);

        text("Medallion: " + taxi.medallion, 150, 600);

        text("Hack License: " + taxi.hackLicense, 550, 600);

        text("Vendor ID: " + taxi.vendorID, 150, 620);

        text("Pickup Datetime: " + taxi.pickupDatetime, 350, 620);

        text("Dropoff Datetime: " + taxi.dropoffDatetime, 670, 620);

        text("Trip Distance: " + taxi.tripDistance, 150, 640);

        text("Trip Time: " + taxi.tripTimeSecs, 480, 640);

        text("Number of Passengers: " + taxi.passengerCount, 760, 640);
      }
    }  

    fill(200, 200, 200);

    rect(0, 590, 110, 60);
    theWidget = widgets.get(4);
    theWidget.draw();
    break;

  case query4:  
    nPoints = dataList.size() - 1;
    points = new GPointsArray(nPoints);

    int[] distCount = new int[] {
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    for (int i = 1; i < nPoints; i++) {
      Data theData = dataList.get(i);
      float tripDist = theData.tripDistance;
      if (tripDist <= 0.5)
        distCount[0]++;
      else if (tripDist <= 1.0)
        distCount[1]++;
      else if (tripDist <= 1.5)
        distCount[2]++;
      else if (tripDist <= 2.0)
        distCount[3]++;
      else if (tripDist <= 2.5)
        distCount[4]++;
      else if (tripDist <= 3.0)
        distCount[5]++;
      else if (tripDist <= 3.5)
        distCount[6]++;
      else if (tripDist <= 4.0)
        distCount[7]++;
      else if (tripDist <= 4.5)
        distCount[8]++;
      else if (tripDist <= 5.0)
        distCount[9]++;
      else
        distCount[10]++;
    }
    for (int i = 0; i < distCount.length; i++) {
      points.add(i, distCount[i]);
    }
    GPlot plot3 = new GPlot(this);
    plot3.setPos(25, 25);
    plot3.setDim(800, 400);
    plot3.setDim(new float[] {
      0, 300
    }
    );

    plot3.getXAxis().setAxisLabelText("Distance");
    plot3.getYAxis().setAxisLabelText("Number of taxis");
    plot3.getTitle().setText("Trip Times Histogram");
    plot3.setPoints(points);
    plot3.startHistograms(GPlot.VERTICAL);
    plot3.getHistogram().setDrawLabels(true);

    plot3.beginDraw();
    plot3.drawBackground();
    plot3.drawBox();
    plot3.drawYAxis();
    plot3.drawXAxis();
    plot3.drawTitle();
    plot3.drawHistograms();
    plot3.endDraw(); 

    fill(200, 200, 200);

    rect(0, 590, 110, 60);
    theWidget = widgets.get(4);
    theWidget.draw();
    break;
  case query5:  
    nPoints = dataList.size() - 1;
    points = new GPointsArray(nPoints);
    int[] passengerCountPerHour = new int[] {
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    for (int i = 1; i < nPoints; i++) {
      Data theData = dataList.get(i);
      int numberOfPassengers = theData.passengerCount;
      int tripHour = theData.getPickupHour();
      if (tripHour == 0)
        passengerCountPerHour[0]++;
      else if (tripHour <= 1)
        passengerCountPerHour[1]++;
      else if (tripHour <= 2)
        passengerCountPerHour[2]++;
      else if (tripHour <= 3)
        passengerCountPerHour[3]++;
      else if (tripHour <= 4)
        passengerCountPerHour[4]++;
      else if (tripHour <= 5)
        passengerCountPerHour[5]++;
      else if (tripHour <= 6)
        passengerCountPerHour[6]++;
      else if (tripHour <= 7)
        passengerCountPerHour[7]++;
      else if (tripHour <= 8)
        passengerCountPerHour[8]++;
      else if (tripHour <= 9)
        passengerCountPerHour[9]++;
      else if (tripHour <= 10)
        passengerCountPerHour[10]++;
      else if (tripHour <= 11)
        passengerCountPerHour[11]++;
      else if (tripHour <= 12)
        passengerCountPerHour[12]++;
      else if (tripHour <= 13)
        passengerCountPerHour[13]++;
      else if (tripHour <= 14)
        passengerCountPerHour[14]++;
      else if (tripHour <= 15)
        passengerCountPerHour[15]++;
      else if (tripHour <= 16)
        passengerCountPerHour[16]++;
      else if (tripHour <= 17)
        passengerCountPerHour[17]++;
      else if (tripHour <= 18)
        passengerCountPerHour[18]++;
      else if (tripHour <= 19)
        passengerCountPerHour[19]++;
      else if (tripHour <= 20)
        passengerCountPerHour[20]++;
      else if (tripHour <= 21)
        passengerCountPerHour[21]++;
      else if (tripHour <= 22)
        passengerCountPerHour[22]++;
      else
        passengerCountPerHour[23]++;
    }
    for (int i = 0; i < passengerCountPerHour.length; i++) {
      points.add(i, passengerCountPerHour[i]);
    }
    GPlot plot4 = new GPlot(this);
    plot4.setPos(25, 25);
    plot4.setDim(800, 400);
    plot4.setDim(new float[] {
      0, 300
    }
    );

    plot4.getXAxis().setAxisLabelText("Hour");
    plot4.getYAxis().setAxisLabelText("Number of passengers");
    plot4.getTitle().setText("Passengers per Hour");
    plot4.setXLim(-0.5, 23.5);
    plot4.setPoints(points);
    plot4.startHistograms(GPlot.VERTICAL);
    plot4.getHistogram().setDrawLabels(true);

    plot4.beginDraw();
    plot4.drawBackground();
    plot4.drawBox();
    plot4.drawYAxis();
    plot4.drawXAxis();
    plot4.drawTitle();
    plot4.drawHistograms();
    plot4.endDraw();

    fill(200, 200, 200);

    rect(0, 590, 110, 60);
    theWidget = widgets.get(4);
    theWidget.draw();
    break;
  case query6:   

    // Some code to set the markers to display on the map (based on currentTime)
    displayTimeMarkers = true;

    if (currentTime < 2300 && count==0)
    {
      map.setCurrentTimeMarkers(this, dataList, currentTime);
    } else if (currentTime < 2300)
    {      
      currentTime = currentTime + 100;
      map.setCurrentTimeMarkers(this, dataList, currentTime);
      count=0;
    } else
    {
      map.setMarkers(this, dataList);
      displayTimeMarkers = false;
    }   

    current_query = query3; 

    map.draw();

    break;
  case query7:

    map.draw();    

    viewTrip = true;  

    if (!markerSelected) {

      PFont g = createFont("Arial", 200, true);
      textFont(g, 13);
      fill(200, 200, 200);
      rect(0, 550, 1000, 100);

      g = createFont("Arial Bold", 200, true);
      textFont(g, 15);
      fill(0);
      text("Click on a Marker or enter a Medallion to view the corresponding taxi's trip data:", 200, 580);

      medallionBox.draw();
      medallionWidget.draw();
     
      if (search_item == medallionSearch)
      {
        ArrayList<Data> newDataList = new ArrayList<Data>();
        String targetMedallion = medallionBox.getText();
        println(targetMedallion);
        for (int i = 0; i < dataList.size (); i++) {
          Data dataItem = dataList.get(i);

          if (dataItem.medallion.equals(targetMedallion))
          {
            newDataList.add(dataItem);
          }
        }
        if (newDataList != null) {
          map = new GradientMap(this, newDataList);
          for (int i=0; i<map.markerList.size (); i++) {
            TaxiMarker marker = map.markerList.get(i);          
            marker.setDisplay(map.taxiLocations);
            println(marker.markerInfo);
            markerSelected = true;
            viewTrip = false;
          }
          current_query = query3;
        } else {
          println("Invalid Medallion, no taxi found.");
        }
      }
    }

    fill(200, 200, 200);
    rect(0, 590, 110, 60);
    theWidget = widgets.get(4);
    theWidget.draw();

    PFont g = createFont("Arial", 200, true);
    textFont(g, 14);
    fill(200, 200, 200);
    rect(0, 590, 110, 60, 0, 15, 0, 0);
    theWidget = widgets.get(4);
    theWidget.draw();

    break;
  }
}
Boolean cleanData(float pickupLongitude, float pickupLatitude, float dropoffLongitude, 
float dropoffLatitude, int passengerCount, int tripTimeSecs, float tripDistance, String...args) {
  if (passengerCount < 0 || tripTimeSecs < 0 || tripDistance < 0 ||
    pickupLongitude == 0 || pickupLatitude == 0 || dropoffLongitude == 0 || dropoffLatitude == 0) {
    return false;
  }

  //  for (String arg : args) {
  //    if (arg == null || arg.equals("")) {
  //      return false;
  //    }
  //  }
  return true;
}

void mouseOverWidget()
{
  Widget currentWidget;
  for (int i = 0; i < widgets.size (); i++)
  { 
    currentWidget = (Widget) widgets.get(i);
    if (mouseX >= currentWidget.x && mouseX < currentWidget.x + currentWidget.width &&
      mouseY >= currentWidget.y && mouseY < currentWidget.y + currentWidget.height)
    {
      currentWidget.hover = true;
    } else {
      currentWidget.hover = false;
    }
  }

  if (mouseX >= medallionWidget.x && mouseX < medallionWidget.x + medallionWidget.width &&
    mouseY >= medallionWidget.y && mouseY < medallionWidget.y + medallionWidget.height)
  {
    medallionWidget.hover = true;
  } else {
    medallionWidget.hover = false;
  }
  
  if (mouseX >= resetSelectWidget.x && mouseX < resetSelectWidget.x + resetSelectWidget.width &&
    mouseY >= resetSelectWidget.y && mouseY < resetSelectWidget.y + resetSelectWidget.height)
  {
    resetSelectWidget.hover = true;
  } else {
    resetSelectWidget.hover = false;
  }
}

