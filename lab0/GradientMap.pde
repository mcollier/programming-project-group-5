import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.providers.*;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;
import de.fhpotsdam.unfolding.marker.MarkerManager;

class GradientMap {
  UnfoldingMap map;
  Location mapLocation;  
  float maxPanningDistance;
  ArrayList<Data> taxiLocations;
  ArrayList<TaxiMarker> markerList;
  MarkerManager<TaxiMarker> manager;
  
  boolean pickupOrDropoff;
  
  public GradientMap(processing.core.PApplet p, ArrayList<Data> dataList) {
    map = new UnfoldingMap(p, new Google.GoogleMapProvider());
    MapUtils.createDefaultEventDispatcher(p, map);
    mapLocation = new Location(40.7127f, -74.0059f);
    map.zoomAndPanTo(mapLocation, 10);
    maxPanningDistance = 30;
    map.setPanningRestriction(mapLocation, maxPanningDistance);
    map.setZoomRange(10, 15);
    taxiLocations = dataList;
    manager = new MarkerManager<TaxiMarker>();
    manager.setMap(map);

    markerList = new ArrayList<TaxiMarker>();

    for (int indexOne = 0; indexOne < taxiLocations.size (); indexOne++)
    {
      Data taxi = taxiLocations.get(indexOne);

      Location pickupLocation = new Location(taxi.pickupLatitude, taxi.pickupLongitude);
      Location dropoffLocation = new Location(taxi.dropoffLatitude, taxi.dropoffLongitude);

      pickupOrDropoff = true;  
      TaxiMarker pickupMarker = new TaxiMarker(pickupLocation, pickupOrDropoff);
      
      pickupOrDropoff = false;
      TaxiMarker dropoffMarker = new TaxiMarker(dropoffLocation, pickupOrDropoff);

      markerList.add(pickupMarker);
      markerList.add(dropoffMarker);

      manager.addMarker(pickupMarker);
      manager.addMarker(dropoffMarker);
    }
  }

  public void setMarkers(processing.core.PApplet p, ArrayList<Data> a) {
    manager.clearMarkers();
    markerList = new ArrayList<TaxiMarker>();

    for (int i=1; i<a.size (); i++) {
      Data dataToDisplay = a.get(i);

      Location pickupLocation = new Location(dataToDisplay.pickupLatitude, dataToDisplay.pickupLongitude);
      Location dropoffLocation = new Location(dataToDisplay.dropoffLatitude, dataToDisplay.dropoffLongitude);

      pickupOrDropoff = true;
      TaxiMarker pickupMarker = new TaxiMarker(pickupLocation, pickupOrDropoff);
      
      pickupOrDropoff = false;
      TaxiMarker dropoffMarker = new TaxiMarker(dropoffLocation, pickupOrDropoff);

      markerList.add(pickupMarker);
      markerList.add(dropoffMarker);

      manager.addMarker(pickupMarker);
      manager.addMarker(dropoffMarker);
    }
  }

  public TaxiMarker getMarker(float latitude, float longitude) {
    for (int i=1; i<markerList.size (); i++) {
      TaxiMarker marker = markerList.get(i);

      if (latitude == marker.getLat() && longitude == marker.getLon())
      {
        return marker;
      }
    }
    return null;
  }

  public void setCurrentTimeMarkers(processing.core.PApplet p, ArrayList<Data> a, int time) {

    manager.clearMarkers();

    for (int i=1; i<a.size (); i++) {
      if (a.get(i).convertPickupTime()>time && a.get(i).convertPickupTime()<(time+100)) {
        Data dataToDisplay = a.get(i);

        Location pickupLocation = new Location(dataToDisplay.pickupLatitude, dataToDisplay.pickupLongitude);
        Location dropoffLocation = new Location(dataToDisplay.dropoffLatitude, dataToDisplay.pickupLongitude);

        pickupOrDropoff = true;
        TaxiMarker pickupMarker = new TaxiMarker(pickupLocation, pickupOrDropoff);
      
        pickupOrDropoff = false;
        TaxiMarker dropoffMarker = new TaxiMarker(dropoffLocation, pickupOrDropoff);

        //markerList.add(pickupMarker);
        //markerList.add(dropoffMarker);

        manager.addMarker(pickupMarker);
        manager.addMarker(dropoffMarker);            
      }
    }
  }

  public TaxiMarker checkMarkers(float x, float y) {

    for (int index=0; index<markerList.size (); index++) {
      TaxiMarker marker = markerList.get(index);
      if (marker != null) {
        if (marker.isInside(map, x, y)) {
          marker.setDisplay(taxiLocations);
          return marker;
        } else
        {
          marker.clearDisplay();
        }
      }
    }
    return null;
  }

  void draw() {
    map.draw();
    manager.draw();
        
    PFont f = createFont("Arial Bold", 200, true);
    textFont(f, 13);
    fill(200, 0, 0);
    ellipse(50, 100, 10, 10);
    fill(255);
    ellipse(50, 100, 5, 5);  
    fill(200, 0, 0);
    text(" = Pickup Point", 55, 105);
    
    fill(0, 0, 200);
    ellipse(50, 120, 10, 10);
    fill(255);
    ellipse(50, 120, 5, 5);
    fill(0, 0, 200);
    text(" = Dropoff Point", 55, 125);
  }
}

