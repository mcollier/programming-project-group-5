class Data { 
  String medallion, hackLicense, vendorID, storeAndFwdFlag, pickupDatetime, dropoffDatetime;
  int rateCode, passengerCount, tripTimeSecs;
  float tripDistance, pickupLongitude, pickupLatitude, dropoffLongitude, dropoffLatitude; 

  Data(String medallion, String hackLicense, String vendorID, int rateCode, String storeAndFwdFlag, 
  String pickupDatetime, String dropoffDatetime, int passengerCount, int tripTimeSecs, 
  float tripDistance, float pickupLongitude, float pickupLatitude, float dropoffLongitude, 
  float dropoffLatitude) {
    this.medallion = medallion;
    this.hackLicense = hackLicense;
    this.vendorID = vendorID;
    this.rateCode = rateCode;
    this.storeAndFwdFlag = storeAndFwdFlag;
    this.pickupDatetime = pickupDatetime;
    this.dropoffDatetime = dropoffDatetime;
    this.passengerCount = passengerCount;
    this.tripTimeSecs = tripTimeSecs;
    this.tripDistance = tripDistance;
    this.pickupLongitude = pickupLongitude;
    this.pickupLatitude = pickupLatitude;
    this.dropoffLongitude = dropoffLongitude;
    this.dropoffLatitude = dropoffLatitude;
  }

  public int getPickupHour() {
    if (pickupDatetime.equals("pickup_datetime"))
    {
      return 0;
    } else return Integer.parseInt(pickupDatetime.substring(11, 13));
  }

  public int convertPickupTime() {
    String data = this.pickupDatetime;
    String[] step1 = data.split(" ");
    String[] step2 = step1[1].split(":");
    int hour = parseInt(step2[0])*100;
    int minute = parseInt(step2[1]);
    int time = hour + minute;
    System.out.println(time);
    return time;
  }

  public int convertDropoffTime() {
    String data = this.dropoffDatetime;
    String[] step1 = data.split(" ");
    String[] step2 = step1[1].split(":");
    int hour = parseInt(step2[0])*100;
    int minute = parseInt(step2[1]);
    int time = hour + minute;
    System.out.println(time);
    return time;
  } 

  public String toString() {
    String dataToString = medallion + ", " + hackLicense + ", " + vendorID + ", " + rateCode + ", " + storeAndFwdFlag + ", " + pickupDatetime      + ", " + dropoffDatetime + ", " + passengerCount + ", " + tripTimeSecs + ", " + tripDistance + ", " + pickupLongitude + ", " +
      pickupLatitude + ", " + dropoffLongitude + ", " + dropoffLatitude;
    return dataToString;
  }
}

