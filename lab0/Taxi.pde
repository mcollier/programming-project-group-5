class Taxi {
  String medallion, hackLicense;
  ArrayList<Data> trips = new ArrayList<Data>();

  Taxi(String medallion, String hackLicense) {
    this.medallion = medallion;
    this.hackLicense = hackLicense;
    this.trips = trips;
  }

  Taxi(String medallion, String hackLicense, Data trip) {
    this.medallion = medallion;
    this.hackLicense = hackLicense;
    this.trips.add(trip);
  }

  public void addTrip(Data trip) {
    this.trips.add(trip);
  }

  public String toString() {
    String dataString = "Medallion: " + medallion + "\n HackLicense: " + hackLicense;
    for (int i = 0; i < this.trips.size (); i++) {
      Data trip = this.trips.get(i);
      dataString += "\n" + trip.toString();
    }
    return dataString;
  }
}

