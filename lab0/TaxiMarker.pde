import processing.core.*;
import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.marker.SimplePointMarker;

public class TaxiMarker extends SimplePointMarker {

  color markerColor;
  String markerInfo;
  boolean displayData = false;

  public TaxiMarker(Location location, boolean pickupOrDropoff) {
    super(location);
    
    if (pickupOrDropoff) {
      markerColor = color(200, 0, 0);
    }
    else
    {
      markerColor = color(0, 0, 200);
    }
  }

  public void setDisplay(ArrayList<Data> dataList) {
    for (int index=0; index < dataList.size (); index++) {
      Data data = dataList.get(index);

      if ((data.pickupLatitude == location.getLat()) && (data.pickupLongitude == location.getLon())) {
        markerInfo = "Pickup Coordinates:" + "\n" + location.getLat() + " " + location.getLon();
        displayData = true;
      } else if ((data.dropoffLatitude == location.getLat()) && (data.dropoffLongitude == location.getLon())) {         
        markerInfo = "Dropoff Coordinates:" + "\n" + location.getLat() + " " + location.getLon(); 
        displayData = true;
      }
    }
  }

  public void clearDisplay() {
    displayData = false;
  }

  public double getLat() {
    return location.getLat();
  }

  public double getLon() {
    return location.getLon();
  }

  @Override
    public void draw(PGraphics pg, float x, float y) {

    pg.pushStyle();
    pg.noStroke();
    pg.fill(markerColor, 100);
    pg.ellipse(x, y, 10, 10);
    pg.fill(255, 100);
    pg.ellipse(x, y, 5, 5);
    pg.popStyle();

    if (displayData) {
      PFont f = createFont("Arial Bold", 200, true);
      textFont(f, 13);
      fill(255);
      rect(x+10, y+10, 100, 100);
      fill(0);
      text(markerInfo, x+15, y, 100, 100);
    }
  }
}

